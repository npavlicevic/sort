#
# Makefile 
# sorting algorithms 
#

CC=dmd
CFLAGS=-H -gc
CFLAGS_LIB=-lib

FILES_QUICK=array/array.d heap/heap.d sort/sort.d main_quick.d
FILES_QUICK_BOUNDRIES=array/array.d heap/heap.d sort/sort.d main_quick_boundries.d
FILES_QUICK_DOUBLE_STACK=array/array.d heap/heap.d sort/sort.d main_quick_double_stack.d
FILES_COMB=array/array.d heap/heap.d sort/sort.d main_comb.d
FILES_BUBBLE=array/array.d heap/heap.d sort/sort.d main_bubble.d
FILES_SHELL=array/array.d heap/heap.d sort/sort.d main_shell.d
FILES_INSERT=array/array.d heap/heap.d sort/sort.d main_insert.d
FILES_SHELL_INSERT=array/array.d heap/heap.d sort/sort.d main_shell_insert.d
FILES_MERGE=array/array.d heap/heap.d sort/sort.d main_merge.d
FILES_SELECT=array/array.d heap/heap.d sort/sort.d main_select.d
FILES_HEAP=array/array.d heap/heap.d sort/sort.d main_heap.d
FILES_LIB_ARRAY=array/array.d
CLEAN=main_quick main_comb main_bubble main_shell main_insert main_shell_insert main_merge main_quick_boundries main_quick_double_stack main_select lib_array main_heap

all: main_quick main_comb main_bubble main_shell main_insert main_shell_insert main_merge main_quick_boundries main_quick_double_stack main_select lib_array main_heap

main_quick: ${FILES_QUICK}
	${CC} $^ -ofmain_quick ${CFLAGS}

main_quick_boundries: ${FILES_QUICK_BOUNDRIES}
	${CC} $^ -ofmain_quick_boundries ${CFLAGS}

main_quick_double_stack: ${FILES_QUICK_DOUBLE_STACK}
	${CC} $^ -ofmain_quick_double_stack ${CFLAGS}

main_comb: ${FILES_COMB}
	${CC} $^ -ofmain_comb ${CFLAGS}

main_bubble: ${FILES_BUBBLE}
	${CC} $^ -ofmain_bubble ${CFLAGS}

main_shell: ${FILES_SHELL}
	${CC} $^ -ofmain_shell ${CFLAGS}

main_insert: ${FILES_INSERT}
	${CC} $^ -ofmain_insert ${CFLAGS}

main_shell_insert: ${FILES_SHELL_INSERT}
	${CC} $^ -ofmain_shell_insert ${CFLAGS}

main_merge: ${FILES_MERGE}
	${CC} $^ -ofmain_merge ${CFLAGS}

main_select: ${FILES_SELECT}
	${CC} $^ -ofmain_select ${CFLAGS}

lib_array: ${FILES_LIB_ARRAY}
	${CC} $^ -oflibArrayD ${CFLAGS} ${CFLAGS_LIB}

main_heap: ${FILES_HEAP}
	${CC} $^ -ofmain_heap ${CFLAGS}

clean: ${CLEAN}
	rm $^
