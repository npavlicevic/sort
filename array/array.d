module array;

import std.random;
import core.stdc.stdio;

void array_swap(int *array, int from, int to) {
  int temp = array[to];
  array[to] = array[from];
  array[from] = temp;
}
void array_print(int *array, int n) {
  int i = 0;
  for(; i < n; i++) {
    printf("%d ", array[i]);
  }
  printf("\n");
}
void array_init(int *array, int n) {
  int i = 0;
  int max = 1024;
  for(; i < n; i++) {
    array[i] = uniform(0, max);
  }
  // TODO make max param;
}
void array_copy(int *dest, int *src, int lo, int hi) {
  int i = lo;
  for(; i < hi; i++) {
    dest[i] = src[i];
  }
}
int compare(T)(T other, T key) {
  if(other < key) {
    return -1;
  } else if(other > key) {
    return 1;
  }
  return 0;
}
