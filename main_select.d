import array;
import sort;

import std.random;
import core.stdc.stdlib;
import core.stdc.stdio;

void main() {
  int n, direction, i = 0;
  fscanf(stdin, "%d", &n);
  fscanf(stdin, "%d", &direction);
  int *array = cast(int*)calloc(n, int.sizeof);
  array_init(array, n);
  array_print(array, n);
  selectsort(array, n, direction, &(compare!int));
  array_print(array, n);
  free(array);
}
