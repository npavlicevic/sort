module heap;

import core.stdc.stdlib;
import array;

template Heap_(T) {
  struct Heap {
    int position;
    int capacity;
    T *items;
  }
}

Heap_!T.Heap *heap_create(T)(int capacity) {
  Heap_!T.Heap *that = cast(Heap_!T.Heap*)calloc(1, Heap_!T.Heap.sizeof);
  that.position = 1;
  that.capacity = capacity;
  that.items = cast(T*)calloc(that.capacity, int.sizeof);
  return that;
}
void heap_push(T, F_COMPARE)(Heap_!T.Heap *that, int item, int direction, F_COMPARE compare) {
  that.items[that.position] = item;
  int child = that.position;
  int parent = child / 2;
  for(;parent > 0;) {
    if(compare(that.items[child], that.items[parent]) == direction) {
      array_swap(that.items, child, parent);
      // TODO add compare and direction
      child = parent;
      parent /= 2;
    } else {
      break;
    }
  }
  that.position++;
}
T heap_pop(T)(Heap_!T.Heap *that) {
  T r = that.items[1];
  int parent = 1;
  int left_child;
  int right_child;
  int direction;
  // 0 left 1 right
  that.items[1] = that.items[--that.position];
  for(;1;) {
    left_child = 2 * parent;
    right_child = 2 * parent + 1;
    if(left_child >= that.position && right_child >= that.position) {
      break;
    }
    if(right_child >= that.position) {
      direction = 0;
    } else if(left_child >= that.position) {
      direction = 1;
    } else if(that.items[left_child] < that.items[right_child]) {
      direction = 0;
      // TODO add direction and comparison
    } else {
      direction = 1;
    }
    if(!direction) {
      if(that.items[left_child] < that.items[parent]) {
        array_swap(that.items, left_child, parent);
        parent = left_child;
      } else {
        break;
      }
    } else {
      if(that.items[right_child] < that.items[parent]) {
        array_swap(that.items, right_child, parent);
        parent = right_child;
      } else {
        break;
      }
    }
  }
  return r;
}
void heap_destroy(T)(Heap_!T.Heap **that) {
  free((*that).items);
  free(*that);
  *that = null;
}
