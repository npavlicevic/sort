import array;
import sort;

import std.random;
import core.stdc.stdlib;
import core.stdc.stdio;

void main() {
  int n, direction, i = 0;
  fscanf(stdin, "%d", &n);
  fscanf(stdin, "%d", &direction);
  int *array = cast(int*)calloc(n, int.sizeof);
  array_init(array, n);
  array_print(array, n);
  quicksort_split_!int.sort_(
    array, 
    0, 
    n-1, 
    direction,
    quicksort_split_!int.split_,
    quicksort_split_!int.compare_);
  array_print(array, n);
  free(array);
}
